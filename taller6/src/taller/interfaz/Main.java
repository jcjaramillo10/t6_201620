package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import taller.estructuras.TablaHash;
import taller.estructuras.ValorHash;
import taller.mundo.Ciudadano;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static TablaHash<String,Ciudadano> tablaNombre = new TablaHash<String, Ciudadano>();
	private static TablaHash<String,Ciudadano> tablaApellido = new TablaHash<String, Ciudadano>();
	private static TablaHash<Integer,Ciudadano> tablaTelefono = new TablaHash<Integer, Ciudadano>();
	private static TablaHash<String ,Ciudadano> tablaUbicacion = new TablaHash<String, Ciudadano>();
	
	public static void main(String[] args) {
		//Cargar registros
		System.out.println("Bienvenido al directorio de emergencias por colisiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				//Recuerde revisar en el enunciado la estructura de la información
				Ciudadano c = new Ciudadano(datos[0], datos[1], Integer.parseInt(datos[2]));
				tablaNombre.put(c.getNombre(), c);
				tablaApellido.put(c.getApellido(), c);
				tablaTelefono.put(c.getTelefono(), c);
				tablaUbicacion.put(c.getLatitud()+ " - " + c.getLongitud(), c);
				++i;
				if (++i%100000 == 0) //5000000
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");
		boolean seguir = true;
		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiente");
				System.out.println("3: Buscar ciudadano por nombre o apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();

				ValorHash <Ciudadano> valor = null;
				switch (in) {
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Nombre del ciudadano a Agregar:");
					String nom = br.readLine();
					System.out.println("Apellido del ciudadano a Agregar:");
					String ape = br.readLine();
					System.out.println("Telefono del acudiente mas cercano (7 números):");
					String tel = br.readLine();
					Ciudadano c = new Ciudadano(nom, ape, Integer.parseInt(tel));
					tablaNombre.put(c.getNombre(), c);
					tablaApellido.put(c.getApellido(), c);
					tablaTelefono.put(c.getTelefono(), c);
					tablaUbicacion.put(c.getLatitud()+ " - " + c.getLongitud(), c);
					System.out.println("el Ciudadano se ha agregado correctamente");
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					System.out.println("Telefono del acudiente mas cercano a buscar:");
					String llave = br.readLine();
					valor = tablaTelefono.get(Integer.parseInt(llave));
					if (valor == null){
						System.out.println("No existe el ciudadano con el nombre buscado");
					}
					else{
						System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
						System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
						System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
								" - " + valor.getValor().getLongitud() );
						while (valor.getSiguiente() != null)
						{
							valor = valor.getSiguiente();
							System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
							System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
							System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
									" - " + valor.getValor().getLongitud() );
						}
					}
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					System.out.println("1. Buscar por nombre.");
					System.out.println("2. Buscar por apellido.");
					String opc = br.readLine();
					switch (opc) {
					case "1":
						System.out.println("Nombre a buscar:");
						String nombre = br.readLine();
						valor = tablaNombre.get(nombre);
						if (valor == null){
							System.out.println("No existe el ciudadano con el nombre buscado");
						}
						else{
							System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
							System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
							System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
									" - " + valor.getValor().getLongitud() );
							while (valor.getSiguiente() != null)
							{
								valor = valor.getSiguiente();
								System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
								System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
								System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
										" - " + valor.getValor().getLongitud() );
							}
						}
						break;
					case "2":
						System.out.println("Apellido a buscar:");
						String apellido = br.readLine();
						valor = tablaApellido.get(apellido);
						if (valor == null){
							System.out.println("No existe el ciudadano con el nombre buscado");
						}
						else{
							System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
							System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
							System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
									" - " + valor.getValor().getLongitud() );
							while (valor.getSiguiente() != null)
							{
								valor = valor.getSiguiente();
								System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
								System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
								System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
										" - " + valor.getValor().getLongitud() );
							}
						}
						break;
					default:
						break;
					}
					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					System.out.println("Ubicación del ciudadano (latitud - longitud)");
					String direccion = br.readLine();
					valor = tablaUbicacion.get(direccion);
					if (valor == null){
						System.out.println("No existe el ciudadano con el nombre buscado");
					}
					else{
						System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
						System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
						System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
								" - " + valor.getValor().getLongitud() );
						while (valor.getSiguiente() != null)
						{
							valor = valor.getSiguiente();
							System.out.println("Nombre: "+ valor.getValor().getNombre() + valor.getValor().getApellido());
							System.out.println("Telefono del acudiente:"+ valor.getValor().getTelefono());
							System.out.println("Localización (latitud - longitud): " + valor.getValor().getLatitud() + 
									" - " + valor.getValor().getLongitud() );
						}
					}
					break;
					
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

}
