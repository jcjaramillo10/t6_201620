package taller.mundo;

import java.util.Random;

public class Ciudadano {

	/**
	 * Nombre del ciudadano
	 */
	private String nombre;

	/**
	 * Apellido del ciudadano
	 */
	private String apellido;

	/**
	 * Telefono del acudiente mas cercano
	 */
	private int telefono;

	/**
	 * Ubicacion del ciudadano, la latitud se encuentra entre 0º y 90º
	 */
	private int latitud;

	/**
	 * Ubicacion del ciudadano, la longitud se encuentra entre 0º y 360º
	 */
	private int longitud;


	/**
	 * Constructor de un ciudadano
	 * @param nombre. Nombre del ciudadano.
	 * @param apellido. Apellido del ciudadano
	 * @param telefono. Telefono del acudiente mas cercano
	 */
	public Ciudadano(String nombre, String apellido, int telefono) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		Random rnd = new Random();
		this.latitud = (int) (rnd.nextDouble() * 90 + 0);
		this.longitud = (int) (rnd.nextDouble() * 360 + 0);
	}

	/**
	 * Retorna la latitud del ciudadano
	 * @return latitud. latitud entre 0º y 90º
	 */
	public int getLatitud() {
		return latitud;
	}

	/**
	 * Retorna la latitud del ciudadano
	 * @return latitud. latitud entre 0º y 90º
	 */
	public int getLongitud() {
		return longitud;
	}
	
	/**
	 * Asigna la latitud y la longitud del ciudadano.
	 * @param latitud. Ubicacion del ciudadano, la latitud se encuentra entre 0º y 90º
	 * @param latitud. Ubicacion del ciudadano, la longitud se encuentra entre 0º y 360º
	 */

	public void setUbicacion(int latitud, int longitud) {
		this.latitud = latitud;
		this.longitud = longitud;
	}

	/**
	 * Retorna el nombre del ciudadano
	 * @return nombre. Nombre del ciudadano
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Retorna el apellido del ciudadano
	 * @return apellido. Apellido del ciudadano
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * Retorna el telefono del acudiente mas cercano
	 * @return telefono. Telefono del acudiente mas cercano
	 */
	public int getTelefono() {
		return telefono;
	}


}
