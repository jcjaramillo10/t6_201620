package taller.estructuras;

import java.util.ArrayList;

public class NodoHash<K,V> {

	private K llave;
	private ValorHash<V> valor;
	private NodoHash<K, V> siguiente;
	
	public NodoHash(K llave, V valor) {
		super();
		this.llave = llave;
		this.valor = new ValorHash<V>(valor);
		siguiente = null;
	}
	
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
	public ValorHash<V> getValor() {
		return this.valor;
	}

	public void setValor(V valor) {
		if (this.valor != valor){
			
		}
		ValorHash<V> actual = this.valor;
		while (actual.getSiguiente() != null){
			actual = actual.getSiguiente();
		}
		actual.setSiguiente(new ValorHash<V>(valor));
	}

	public NodoHash<K, V> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoHash<K, V> siguiente) {
		this.siguiente = siguiente;
	}
	
}
