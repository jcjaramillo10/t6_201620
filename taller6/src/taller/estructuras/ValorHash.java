package taller.estructuras;

public class ValorHash<V> {
	
	private V valor;
	private ValorHash<V> siguiente;
	
	public ValorHash(V valor) {
		super();
		this.valor = valor;
		siguiente = null;
	}

	public V getValor() {
		return valor;
	}

	public void setValor(V valor) {
		this.valor = valor;
	}
	
	public ValorHash<V> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(ValorHash<V> siguiente) {
		this.siguiente = siguiente;
	}
	

}
