package taller.estructuras;

import java.util.ArrayList;


public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeración que representa los tipos de colisiones que se pueden manejar

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	//TODO: Agruegue la estructura que va a soportar la tabla.
	private NodoHash<K, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;
	

	//Constructores

	@SuppressWarnings("unchecked")
	public TablaHash(){
		//TODO: Inicialice la tabla con los valores que considere prudentes para una ejecución normal
		this.capacidad = 100000;
		this.tabla = new NodoHash[capacidad];
		this.factorCargaMax = (float) 4.0;
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		//TODO: Inicialice la tabla con los valores dados por parametro
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		this.tabla = new NodoHash[capacidad];
	}

	public void put(K llave, V valor){
		//TODO: Guarde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
		if(tabla[pos] == null)
			tabla[pos] = new NodoHash<K, V>(llave, valor);
		else{
			NodoHash<K, V> nodo = tabla[pos];
			while (nodo.getSiguiente() != null && nodo.getLlave() != llave){
				nodo = nodo.getSiguiente();
			}
			if (nodo.getSiguiente() == null)
				nodo.setSiguiente(new NodoHash<K, V>(llave, valor));
			else
				nodo.setValor(valor);
		}
		count++;
		calcularFactorCarga();
	}

	public ValorHash<V> get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		if(tabla[pos] == null)
			return null;
		else{
			NodoHash<K, V> nodo = tabla[pos];
			if (nodo.getLlave() == llave)
				return nodo.getValor();
			NodoHash<K, V> buscado = null;
			boolean encontro = false;
			while (nodo.getSiguiente() != null && !encontro){
				if (nodo.getLlave() == llave){
					buscado = nodo;
					encontro = true;
				}
				nodo = nodo.getSiguiente();
			}
			if (buscado != null)
				return buscado.getValor();
			else
				return null;
		}
	}

	public ValorHash<V> delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		int pos = hash(llave);
		if(tabla[pos] == null)
			return null;
		else{
			NodoHash<K, V> nodo = tabla[pos];
			if (nodo.getLlave() == llave )
			{
				tabla[pos] = nodo.getSiguiente();
				count--;
				calcularFactorCarga();
				return nodo.getValor();
			}
			while (nodo.getSiguiente() != null && nodo.getSiguiente().getLlave() != llave){
				nodo = nodo.getSiguiente();
			}
			if (nodo.getSiguiente() == null)
				return null;
			else{
				count--;
				calcularFactorCarga();
				ValorHash<V> eliminar = nodo.getSiguiente().getValor();
				nodo.setSiguiente(nodo.getSiguiente().getSiguiente());
				return eliminar;
			}
				
		}
	}

	//Hash
	private int hash(K llave)
	{
		//TODO: Escriba una función de Hash, recuerde tener en cuenta la complejidad de ésta así como las colisiones.
		int valor = 0;
		for (int i = 0; i < llave.toString().length(); i++)
		{
			valor += llave.toString().codePointAt(i);
		}
		return valor % capacidad;
	}
	
		//TODO: Permita que la tabla sea dinamica

	private void calcularFactorCarga()
	{
		factorCarga = count/capacidad;
		if (factorCarga > factorCargaMax)
		{
			NodoHash<K, V>[] vieja = tabla;
			capacidad += capacidad;
			this.tabla = new NodoHash[capacidad];
			for (int i=0; i<vieja.length;i++)
			{                     
				if(vieja[i] != null){
					NodoHash<K, V> actual = vieja[i];
					ValorHash<V> valor = actual.getValor();
					put(actual.getLlave(), valor.getValor());
					while (valor.getSiguiente() != null)
					{
						valor = valor.getSiguiente();
						put(actual.getLlave(), valor.getValor());
					}
					
				}
			}
		}
	}

}